package pl.bezpiekaguys.myapplication.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import pl.bezpiekaguys.myapplication.quantity.AlcoholKind

// TODO https://developer.android.com/training/data-storage/room/ jako encja + dodać DAO+DB

@Entity(tableName = "alcohol_quantity_table")
data class AlcoholQuantity(
    @PrimaryKey(autoGenerate = true)
    val alcoholID : Long = 0L,

    @ColumnInfo(name = "alcohol_kind")
    val alcoholKind: AlcoholKind,

    @ColumnInfo(name = "alcohol_content")
    val alcoholContent: Double,

    @ColumnInfo(name = "amount_consumed")
    val amountConsumed: Double
)
