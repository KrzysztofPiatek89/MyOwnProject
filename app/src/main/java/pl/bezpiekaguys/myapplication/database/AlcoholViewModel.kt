package pl.bezpiekaguys.myapplication.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlcoholViewModel(application: Application) :AndroidViewModel(application) {

    val readAllData: LiveData<List<AlcoholQuantity>>
    private val repository: AlcoholRepository

    init {
        val alcoholDatabaseDao = AlcoholDatabase.getInstance(application).alcoholDatabaseDao()
        repository = AlcoholRepository(alcoholDatabaseDao)
        readAllData = repository.readAllData
    }
    fun addAlcohol(alcohol: AlcoholQuantity){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addAlcohol(alcohol)
        }
    }
}