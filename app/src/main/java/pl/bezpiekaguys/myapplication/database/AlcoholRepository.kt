package pl.bezpiekaguys.myapplication.database

import androidx.lifecycle.LiveData

class AlcoholRepository (private val alcoholDatabaseDao: AlcoholDatabaseDao) {

    val readAllData: LiveData<List<AlcoholQuantity>> = alcoholDatabaseDao.readAllData()

    suspend fun addAlcohol(alcohol: AlcoholQuantity){
        alcoholDatabaseDao.addAlcohol(alcohol)
    }
}