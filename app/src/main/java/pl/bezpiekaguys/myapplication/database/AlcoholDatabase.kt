package pl.bezpiekaguys.myapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [AlcoholQuantity::class],version = 1,exportSchema = false)
abstract class AlcoholDatabase: RoomDatabase(){

//    abstract val alcoholDatabaseDao:AlcoholDatabaseDao

    // w jednym z poradnikow tak w drugim tak

    abstract fun alcoholDatabaseDao():AlcoholDatabaseDao

    companion object{
        @Volatile
        private var INSTANCE:AlcoholDatabase? = null

        fun getInstance(context: Context): AlcoholDatabase{
            synchronized(this){
                var instance = INSTANCE

                if (instance==null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AlcoholDatabase::class.java,
                        "alcohol_consumed_database")
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}