package pl.bezpiekaguys.myapplication.database

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface AlcoholDatabaseDao {
    @Query("SELECT * FROM alcohol_quantity_table")
    fun getAll(): List<AlcoholQuantity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addAlcohol(alcoholQuantity: AlcoholQuantity)

    @Update
    fun update(alcoholQuantity: AlcoholQuantity)

    @Delete
    fun delete(alcoholQuantity: AlcoholQuantity)

    @Query("SELECT * FROM alcohol_quantity_table WHERE alcoholID = :key")
    fun get(key: Long): AlcoholQuantity

    @Query("SELECT * FROM alcohol_quantity_table ORDER BY alcoholID ASC")
    fun readAllData(): LiveData<List<AlcoholQuantity>>
}

