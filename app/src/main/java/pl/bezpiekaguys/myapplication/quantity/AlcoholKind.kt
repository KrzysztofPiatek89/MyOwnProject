package pl.bezpiekaguys.myapplication.quantity

enum class AlcoholKind {
    Piwo, Wodka, Tequila, Spirytus, Bimber, Gin, Rum
}