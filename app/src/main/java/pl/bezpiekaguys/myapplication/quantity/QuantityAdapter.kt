package pl.bezpiekaguys.myapplication.quantity

import android.annotation.SuppressLint
import pl.bezpiekaguys.myapplication.R
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import pl.bezpiekaguys.myapplication.database.AlcoholQuantity

// https://github.com/CymChad/BaseRecyclerViewAdapterHelper/blob/master/readme/1-BaseQuickAdapter.md
class QuantityAdapter : BaseQuickAdapter<AlcoholQuantity, BaseViewHolder>(R.layout.quantity_item) {

    private var alcoholList = emptyList<AlcoholQuantity>()

    override fun convert(holder: BaseViewHolder, item: AlcoholQuantity) {
        holder.setText(R.id.alcohol_content_item, item.alcoholContent.toString())
        holder.setText(R.id.alcohol_consumed_item, item.amountConsumed.toString())
        holder.setText(R.id.alcohol_kind_item, item.alcoholKind.toString())

        // TODO RESZTA PÓL!!!
    }
//    @SuppressLint("NotifyDataSetChanged")
//    fun setData(alcohol: List<AlcoholQuantity>){
//        this.alcoholList = alcohol
//        notifyDataSetChanged()
//    }
}