package pl.bezpiekaguys.myapplication.quantity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import pl.bezpiekaguys.myapplication.database.AlcoholDatabase
import pl.bezpiekaguys.myapplication.database.AlcoholDatabaseDao
import pl.bezpiekaguys.myapplication.database.AlcoholQuantity
import pl.bezpiekaguys.myapplication.database.AlcoholViewModel
import pl.bezpiekaguys.myapplication.databinding.FragmentAlcoholQuantityBinding

class AlcoholQuantityFragment : Fragment() {

    private lateinit var binding: FragmentAlcoholQuantityBinding
    var alkoholInBloodAfterResting: Double = 0.0
    private lateinit var alcoholViewModel: AlcoholViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAlcoholQuantityBinding.inflate(inflater, container, false)


        val quantityAdapter = QuantityAdapter()

        binding.list.layoutManager = LinearLayoutManager(requireContext())
        binding.list.adapter = quantityAdapter

        // TODO POBRAC Z BAZY DANYCH ROOM!!! https://developer.android.com/training/data-storage/room/
        alcoholViewModel = ViewModelProvider(this).get(AlcoholViewModel::class.java)
        alcoholViewModel.readAllData.observe(viewLifecycleOwner, Observer { alcohol ->
            quantityAdapter.setNewInstance(alcohol as MutableList<AlcoholQuantity>?)
        })

        binding.add.setOnClickListener {
            // TODO dodać przejście do fragmentu, który ma edit texty i tam to wpisać i zapisać w ROOM!
            requireView().findNavController().navigate(AlcoholQuantityFragmentDirections.actionAlcoholQuantityFragmentToAddAlcoholFragment())
        }



        binding.sum.setOnClickListener{
            if (alkoholInBloodAfterResting == 0.0) {
                requireView().findNavController()
                    .navigate(AlcoholQuantityFragmentDirections.actionAlcoholQuantityFragmentToSoberFragment())
            } else if (alkoholInBloodAfterResting < 0.0){
                requireView().findNavController()
                    .navigate(AlcoholQuantityFragmentDirections.actionAlcoholQuantityFragmentToSoberFragment())
            } else {
                requireView().findNavController()
                    .navigate(AlcoholQuantityFragmentDirections.actionAlcoholQuantityFragmentToDrunkFragment())
            }
        }

        return binding.root
    }

    private fun getTestData() = mutableListOf(
        AlcoholQuantity(
            alcoholKind = AlcoholKind.Piwo,
            alcoholContent = 3.0,
            amountConsumed = 500.0
        ),
        AlcoholQuantity(
            alcoholKind = AlcoholKind.Piwo,
            alcoholContent = 4.5,
            amountConsumed = 1000.0
        ),
        AlcoholQuantity(
            alcoholKind = AlcoholKind.Piwo,
            alcoholContent = 6.0,
            amountConsumed = 500.0
        ),
        AlcoholQuantity(
            alcoholKind = AlcoholKind.Piwo,
            alcoholContent = 7.2,
            amountConsumed = 500.0
        )
    )
}