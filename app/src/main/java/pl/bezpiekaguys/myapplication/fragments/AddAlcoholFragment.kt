package pl.bezpiekaguys.myapplication.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import pl.bezpiekaguys.myapplication.R
import pl.bezpiekaguys.myapplication.databinding.FragmentAddAlcoholBinding

import pl.bezpiekaguys.myapplication.quantity.AlcoholKind
import pl.bezpiekaguys.myapplication.database.AlcoholQuantity
import pl.bezpiekaguys.myapplication.database.AlcoholViewModel

class AddAlcoholFragment: Fragment() {

    private lateinit var _binding: FragmentAddAlcoholBinding
    private val binding get() = _binding
    private lateinit var alcoholViewModel: AlcoholViewModel

    var addAlcoholKind= ""
    var addAlcoholContent=0.0
    var addAmountConsumed=0.0

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddAlcoholBinding.inflate(inflater, container, false)

        alcoholViewModel = ViewModelProvider(this).get(AlcoholViewModel::class.java)

        binding.addButton.setOnClickListener {
            if (!TextUtils.isEmpty(binding.addAlcoholType.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholConsumedEditText.text.toString()) &&
                !TextUtils.equals(binding.addAlcoholType.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholConsumedEditText.text.toString(),".")
            ){
                addAlcoholKind = binding.addAlcoholType.text.toString()
                addAlcoholContent = binding.alcoholContentEditText.text.toString().toDouble()
                addAmountConsumed = binding.alcoholConsumedEditText.text.toString().toDouble()
            }

            val values: Array<AlcoholKind> = AlcoholKind.values()

            if(values.contains(AlcoholKind.valueOf(addAlcoholKind))){

                val alcohol = AlcoholQuantity(
                    alcoholKind = AlcoholKind.valueOf(addAlcoholKind),
                    alcoholContent = addAlcoholContent,
                    amountConsumed = addAmountConsumed
                )
                alcoholViewModel.addAlcohol(alcohol)

                Toast.makeText(requireContext(),"Pomyślnie dodany",Toast.LENGTH_SHORT).show()
                requireView().findNavController().navigate(AddAlcoholFragmentDirections.actionAddAlcoholFragmentToAlcoholQuantityFragment())
            } else {
                Toast.makeText(requireContext(),"Lipa nie zotalo dodane",Toast.LENGTH_SHORT).show()
            }
        }
        return binding.root
    }


}