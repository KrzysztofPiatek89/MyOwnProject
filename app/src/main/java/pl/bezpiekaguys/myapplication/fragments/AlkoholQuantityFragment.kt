package pl.bezpiekaguys.myapplication.fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import pl.bezpiekaguys.myapplication.personal.PersonalMetods
import pl.bezpiekaguys.myapplication.personal.PersonalMetods.Companion.personalData
import pl.bezpiekaguys.myapplication.R
import pl.bezpiekaguys.myapplication.databinding.FragmentAlkoholQuantityBinding
import pl.bezpiekaguys.myapplication.drunk.DataSavedForDrunkPeople

class AlkoholQuantityFragment : Fragment() {

    private lateinit var _binding: FragmentAlkoholQuantityBinding
    private val binding get() = _binding
    var alkoholInBloodQuantity: Double = 0.0
    var alkoholInBloodValue: Double = 0.0
    var alkoholInBloodAfterResting: Double = 0.0
    var remainingTimeOfSoberingUp: Double = 0.0
    var dataSavedForDrunkPeople = DataSavedForDrunkPeople

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAlkoholQuantityBinding.inflate(inflater,container,false)

        binding.next2Button.setOnClickListener{view:View ->
            if (!TextUtils.isEmpty(binding.alcoholContentEditText1.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText2.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText3.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText4.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText5.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText6.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText7.text.toString()) &&
                !TextUtils.isEmpty(binding.alcoholContentEditText8.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText1.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText2.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText3.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText4.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText5.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText6.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText7.text.toString()) &&
                !TextUtils.isEmpty(binding.litersNumberEditText8.text.toString()) &&
                !TextUtils.equals(binding.alcoholContentEditText1.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText2.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText3.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText4.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText5.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText6.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText7.text.toString(),".") &&
                !TextUtils.equals(binding.alcoholContentEditText8.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText1.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText2.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText3.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText4.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText5.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText6.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText7.text.toString(),".") &&
                !TextUtils.equals(binding.litersNumberEditText8.text.toString(),".")) {

                alkoholInBloodQuantity =
                            ((binding.alcoholContentEditText1.text.toString().toDouble()/100) * binding.litersNumberEditText1.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText2.text.toString().toDouble()/100) * binding.litersNumberEditText2.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText3.text.toString().toDouble()/100) * binding.litersNumberEditText3.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText4.text.toString().toDouble()/100) * binding.litersNumberEditText4.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText5.text.toString().toDouble()/100) * binding.litersNumberEditText5.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText6.text.toString().toDouble()/100) * binding.litersNumberEditText6.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText7.text.toString().toDouble()/100) * binding.litersNumberEditText7.text.toString().toDouble() * 0.8) +
                            ((binding.alcoholContentEditText8.text.toString().toDouble()/100) * binding.litersNumberEditText8.text.toString().toDouble() * 0.8)

                personalData.alcoholQuantity = alkoholInBloodQuantity
                alkoholInBloodValue = PersonalMetods.calculateAlcoholInBloodValue()
                alkoholInBloodAfterResting = PersonalMetods.calculateRemainingAlcoholInBlood(alkoholInBloodValue)
                remainingTimeOfSoberingUp = PersonalMetods.calculateRemainingTimeOfBeingDrunk(alkoholInBloodAfterResting)

                dataSavedForDrunkPeople.valueOfAlkohol = alkoholInBloodAfterResting
                dataSavedForDrunkPeople.remainingTimeOfSobering = remainingTimeOfSoberingUp

                val roundedAlcohol = String.format("%.2f",alkoholInBloodValue)

                if (alkoholInBloodAfterResting == 0.0) {
                    view.findNavController().navigate(AlkoholQuantityFragmentDirections.actionAlkoholQuantityFragmentToSoberFragment())
                    Toast.makeText(context, "alkohol we krwi zaraz po wypiciu wynosił: $roundedAlcohol promili", Toast.LENGTH_LONG).show()
                } else if(alkoholInBloodAfterResting < 0.0){
                    view.findNavController().navigate(AlkoholQuantityFragmentDirections.actionAlkoholQuantityFragmentToSoberFragment())
                    Toast.makeText(context, "alkohol we krwi zaraz po wypiciu wynosił: $roundedAlcohol promili", Toast.LENGTH_LONG).show()
                } else {
                    view.findNavController().navigate(AlkoholQuantityFragmentDirections.actionAlkoholQuantityFragmentToDrunkFragment())
                    Toast.makeText(context, "alkohol we krwi zaraz po wypiciu wynosił: $roundedAlcohol promili", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(context, "Wprowadz poprawne dane (brak pustych miejsc i samych .)", Toast.LENGTH_LONG).show()
            }
        }
        return binding.root
    }

}