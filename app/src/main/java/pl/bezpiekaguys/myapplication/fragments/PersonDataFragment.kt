package pl.bezpiekaguys.myapplication.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import pl.bezpiekaguys.myapplication.personal.PersonalMetods.Companion.personalData
import pl.bezpiekaguys.myapplication.R
import pl.bezpiekaguys.myapplication.databinding.FragmentPersonDataBinding

class PersonDataFragment : Fragment() {

    private lateinit var _binding: FragmentPersonDataBinding
    private val binding get() = _binding
    var sex: Boolean = false
    var weight: Double = 0.0
    var restingTime: Double = 0.0
    var waterInBodyPercent: Double = 0.0

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPersonDataBinding.inflate(inflater, container, false)

        binding.womanButton.setOnClickListener {
            if (binding.manButton.isChecked) {
                binding.manButton.toggle()
            }
        }

        binding.manButton.setOnClickListener {
            if (binding.womanButton.isChecked) {
                binding.womanButton.toggle()
            }
        }

        binding.next1Button.setOnClickListener {
            if ((binding.manButton.isChecked || binding.womanButton.isChecked) &&
                !TextUtils.isEmpty(binding.weightEditText.text.toString()) &&
                !TextUtils.isEmpty(binding.restingTimeEditText.text.toString()) &&
                !TextUtils.equals(binding.weightEditText.text.toString(), ".") &&
                !TextUtils.equals(binding.restingTimeEditText.text.toString(), ".")
            ) {

                if (binding.manButton.isChecked) {
                    sex = true
                    waterInBodyPercent = 0.7
                } else if (binding.womanButton.isChecked) {
                    sex = false
                    waterInBodyPercent = 0.6
                }
                weight = binding.weightEditText.text.toString().toDouble()
                restingTime = binding.restingTimeEditText.text.toString().toDouble()

                personalData.sex = sex
                personalData.weight = weight
                personalData.restingTime = restingTime
                personalData.waterInBodyPercent = waterInBodyPercent

                requireView().findNavController().navigate(PersonDataFragmentDirections.actionPersonDataFragmentToAlcoholQuantityFragment())

                Toast.makeText(
                    context,
                    "Waga: ${weight} i czas: ${restingTime}, wody w ciele: ${waterInBodyPercent}",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    context,
                    "Wybierz płeć, wprowadź wagę i czas od zakończenia picia",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            view?.findNavController()!!
        )
                || super.onOptionsItemSelected(item)
    }
}