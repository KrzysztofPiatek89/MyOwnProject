package pl.bezpiekaguys.myapplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.bezpiekaguys.myapplication.R
import pl.bezpiekaguys.myapplication.databinding.FragmentSoberBinding

class SoberFragment : Fragment() {

    private lateinit var binding: FragmentSoberBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSoberBinding.inflate(inflater,container,false)

        return binding.root
    }
}