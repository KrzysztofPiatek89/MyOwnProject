package pl.bezpiekaguys.myapplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.bezpiekaguys.myapplication.databinding.FragmentDrunkBinding
import pl.bezpiekaguys.myapplication.drunk.DataSavedForDrunkPeople
import pl.bezpiekaguys.myapplication.drunk.DrunkData

class DrunkFragment : Fragment() {

    private lateinit var binding: FragmentDrunkBinding
    var dataSavedForDrunkPeople = DataSavedForDrunkPeople
//    var args: DrunkFragmentArgs.fromBundle(arguments)

    var roundedValue = String.format("%.2f",dataSavedForDrunkPeople.valueOfAlkohol)
    var drunkData = DrunkData("Posidasz " + roundedValue + " promili alkoholu we krwi.",
        "Będziesz całkowicie trzeźwy za " + dataSavedForDrunkPeople.remainingTimeOfSobering.toString() + " godzin od zakończenia picia.")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentDrunkBinding.inflate(inflater,container,false)
        binding.drunkData = drunkData
        binding.lifecycleOwner = this

        return binding.root
        }
    }