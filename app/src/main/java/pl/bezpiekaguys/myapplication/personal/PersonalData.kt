package pl.bezpiekaguys.myapplication.personal

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PersonalData(
    var sex: Boolean,
    var weight: Double,
    var alcoholQuantity: Double,
    var restingTime: Double,
    var waterInBodyPercent: Double
) : Parcelable {

}
