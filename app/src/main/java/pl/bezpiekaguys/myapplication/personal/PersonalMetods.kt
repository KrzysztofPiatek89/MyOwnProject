package pl.bezpiekaguys.myapplication.personal

class PersonalMetods {
    companion object {
        var personalData = PersonalData(false,0.0,0.0,0.0,0.0)
        var burningAlkoholPerHour: Double ?= null

        fun calculateAlcoholInBloodValue(): Double {
            return personalData.alcoholQuantity / (personalData.waterInBodyPercent * personalData.weight)
        }

        fun calculateRemainingAlcoholInBlood(alkoholInBloodValue: Double):Double{
            if (personalData.waterInBodyPercent == 0.7){
                burningAlkoholPerHour = 0.13 // spalanie 0.13 promila alkoholu na godzine w przypadku mezczyzn
            } else {
                burningAlkoholPerHour = 0.8 // spalanie 0.9 promila alkoholu na godzine w przypadku kobiet
            }
            return (alkoholInBloodValue - (burningAlkoholPerHour!! * personalData.restingTime))
        }

        fun calculateRemainingTimeOfBeingDrunk(alkoholInBloodAfterResting: Double):Double{
            var timeOfBurningAlkohol = 0.0
            var alkoholAfterRest: Double = alkoholInBloodAfterResting

                while (alkoholAfterRest > 0.0){
                    alkoholAfterRest -= (burningAlkoholPerHour!!)
                    timeOfBurningAlkohol ++ // czas bedzie z zapasem do nawet godziny by bylo bezpiecznie
                }
            return timeOfBurningAlkohol
        }
    }
}